package neurofuzzy.provider

import neurofuzzy.function.BinaryFunction
import neurofuzzy.function.Operations
import neurofuzzy.neurofuzzynetwork.*

class NeuroFuzzyProviderImpl : NeuroFuzzyProvider {

    override fun tNorm(): BinaryFunction = Operations.product()

    override fun antecedentParamsFactory(): AntecedentParamsFactory = RandomAntecedentParamsFactory()

    override fun consequentParamsFactory(): ConsequentParamsFactory = RandomConsequentParamsFactory()

    override fun antecedentTruthValue(): AntecedentTruthValue = AntecedentSigmoidTruthValue()

    override fun consequentTruthValue(): ConsequentTruthValue = ConsequentLinearTruthValue()

    override fun outputError(): NeuroFuzzyOutputError = NeuroFuzzyOutputErrorImpl()

    override fun error(): NeuroFuzzyError = NeuroFuzzyErrorImpl()
}
