package neurofuzzy.provider

import neurofuzzy.function.BinaryFunction
import neurofuzzy.neurofuzzynetwork.*

interface NeuroFuzzyProvider {

    fun tNorm(): BinaryFunction

    fun antecedentParamsFactory(): AntecedentParamsFactory

    fun consequentParamsFactory(): ConsequentParamsFactory

    fun antecedentTruthValue(): AntecedentTruthValue

    fun consequentTruthValue(): ConsequentTruthValue

    fun outputError(): NeuroFuzzyOutputError

    fun error(): NeuroFuzzyError
}
