package neurofuzzy.neurofuzzynetwork

interface AntecedentTruthValue {

    operator fun invoke(x: Double, a: Double, b: Double): Double
}
