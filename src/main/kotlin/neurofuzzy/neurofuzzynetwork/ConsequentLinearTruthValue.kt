package neurofuzzy.neurofuzzynetwork

import java.lang.IllegalArgumentException

class ConsequentLinearTruthValue : ConsequentTruthValue {

    override fun invoke(inputs: List<Double>, params: List<Double>, r: Double): Double {
        if (inputs.size != params.size) {
            throw IllegalArgumentException("Inputs size ${inputs.size} does not match params size ${params.size}.")
        }

        return inputs.zip(params).map { it.first * it.second }.reduce(Double::plus) + r
    }
}
