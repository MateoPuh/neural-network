package neurofuzzy.neurofuzzynetwork

import neurofuzzy.data.NeuroFuzzyOutput
import neurofuzzy.data.NeuroFuzzyTarget

interface NeuroFuzzyOutputError {

    operator fun invoke(output: NeuroFuzzyOutput, target: NeuroFuzzyTarget): Double
}
