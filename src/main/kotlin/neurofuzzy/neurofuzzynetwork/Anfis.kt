package neurofuzzy.neurofuzzynetwork

import neurofuzzy.data.NeuroFuzzyInput
import neurofuzzy.data.NeuroFuzzyOutput
import neurofuzzy.data.NeuroFuzzyTarget
import neurofuzzy.function.BinaryFunction

class Anfis(
    private val numberOfRules: Int,
    private val antecedentParamsFactory: AntecedentParamsFactory,
    private val consequentParamsFactory: ConsequentParamsFactory,
    private val tNorm: BinaryFunction,
    private val antecedentTruthValue: AntecedentTruthValue,
    private val consequentTruthValue: ConsequentTruthValue,
    private val numberOfInputs: Int = 2
) {
    private val antecedentParams: List<List<AntecedentParams>> =
        (0 until numberOfRules).map { (0 until numberOfInputs).map { antecedentParamsFactory() } }
    private val consequentParams: List<ConsequentParams> =
        (0 until numberOfRules).map { consequentParamsFactory(numberOfInputs) }

    fun output(input: NeuroFuzzyInput): NeuroFuzzyOutput {
        val x = input.inputs[0]
        val y = input.inputs[1]

        val pripadnosti: List<Pair<Double, Double>> = (0 until numberOfRules).map { ruleIndex ->
            Pair(
                antecedentTruthValue(x, antecedentParams[ruleIndex][0].a, antecedentParams[ruleIndex][0].b),
                antecedentTruthValue(y, antecedentParams[ruleIndex][1].a, antecedentParams[ruleIndex][1].b)
            )
        }

        val w = pripadnosti.mapIndexed { ruleIndex, pair -> tNorm(pair.first, pair.second) }

        val f = (0 until numberOfRules).map { ruleIndex ->
            consequentTruthValue(input.inputs, consequentParams[ruleIndex].params, consequentParams[ruleIndex].r)
        }

        val wSum = w.reduce(Double::plus)
        val wfSum = w.zip(f).map { it.first * it.second }.reduce(Double::plus)

        return NeuroFuzzyOutput(wfSum / wSum, pripadnosti, w, f)
    }

    fun update(
        inputs: List<NeuroFuzzyInput>,
        outputs: List<NeuroFuzzyOutput>,
        target: List<NeuroFuzzyTarget>,
        learningRate: Double
    ) {
        val pDelta = DoubleArray(numberOfRules) { 0.0 }.toMutableList()
        val qDelta = DoubleArray(numberOfRules) { 0.0 }.toMutableList()
        val rDelta = DoubleArray(numberOfRules) { 0.0 }.toMutableList()
        val aDelta = DoubleArray(numberOfRules) { 0.0 }.toMutableList()
        val bDelta = DoubleArray(numberOfRules) { 0.0 }.toMutableList()
        val cDelta = DoubleArray(numberOfRules) { 0.0 }.toMutableList()
        val dDelta = DoubleArray(numberOfRules) { 0.0 }.toMutableList()

        outputs.zip(target).mapIndexed { index, (output, target) ->
            val input = inputs[index]
            val x = input.inputs[0]
            val y = input.inputs[1]

            val wSum = output.w.reduce(Double::plus)

            val outputDelta = output.output - target.output
            output.w.forEachIndexed { index, w -> pDelta[index] += outputDelta * w / wSum * x }
            output.w.forEachIndexed { index, w -> qDelta[index] += outputDelta * w / wSum * y }
            output.w.forEachIndexed { index, w -> rDelta[index] += outputDelta * w / wSum }

            val wSquaredSum = output.w.map { it * it }.reduce(Double::plus)

            val middleSum = (0 until numberOfRules).map { ruleIndex ->
                (0 until numberOfRules).map { j ->
                    if (wSquaredSum < 1e-6) {
                        0.0
                    } else {
                        (output.w[j] * (output.f[ruleIndex] - output.f[j])) / wSquaredSum
                    }
                }.reduce(Double::plus)
            }

            (0 until numberOfRules).forEach { ruleIndex ->
                val alpha = output.pripadnosti[ruleIndex].first
                val beta = output.pripadnosti[ruleIndex].second
                val b = antecedentParams[ruleIndex][0].b

                aDelta[ruleIndex] += outputDelta * middleSum[ruleIndex] * alpha * (1 - alpha) * beta * b
            }

            (0 until numberOfRules).forEach { ruleIndex ->
                val alpha = output.pripadnosti[ruleIndex].first
                val beta = output.pripadnosti[ruleIndex].second
                val a = antecedentParams[ruleIndex][0].a

                bDelta[ruleIndex] += outputDelta * middleSum[ruleIndex] * alpha * (1 - alpha) * beta * (a - x)
            }

            (0 until numberOfRules).forEach { ruleIndex ->
                val alpha = output.pripadnosti[ruleIndex].first
                val beta = output.pripadnosti[ruleIndex].second
                val d = antecedentParams[ruleIndex][1].b

                cDelta[ruleIndex] += outputDelta * middleSum[ruleIndex] * alpha * (1 - beta) * beta * d
            }

            (0 until numberOfRules).forEach { ruleIndex ->
                val alpha = output.pripadnosti[ruleIndex].first
                val beta = output.pripadnosti[ruleIndex].second
                val c = antecedentParams[ruleIndex][1].a

                dDelta[ruleIndex] += outputDelta * middleSum[ruleIndex] * alpha * (1 - beta) * beta * (c - y)
            }
        }

        pDelta.forEachIndexed { index, d ->
            consequentParams[index].params[0] -= learningRate * d
        }

        qDelta.forEachIndexed { index, d ->
            consequentParams[index].params[1] -= learningRate * d
        }

        rDelta.forEachIndexed { index, d ->
            consequentParams[index].r -= learningRate * d
        }

        aDelta.forEachIndexed { index, d ->
            antecedentParams[index][0].a -= learningRate * d
        }

        bDelta.forEachIndexed { index, d ->
            antecedentParams[index][0].b -= learningRate * d
        }

        cDelta.forEachIndexed { index, d ->
            antecedentParams[index][1].a -= learningRate * d
        }

        dDelta.forEachIndexed { index, d ->
            antecedentParams[index][1].b -= learningRate * d
        }
    }
}
