package neurofuzzy.neurofuzzynetwork

import kotlin.random.Random

class RandomAntecedentParamsFactory : AntecedentParamsFactory {

    override fun invoke(): AntecedentParams = AntecedentParams(Random.nextDouble() * 2 - 1, Random.nextDouble() * 2 - 1)
}
