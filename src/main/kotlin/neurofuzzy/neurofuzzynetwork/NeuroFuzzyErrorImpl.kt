package neurofuzzy.neurofuzzynetwork

import neurofuzzy.data.NeuroFuzzyOutput
import neurofuzzy.data.NeuroFuzzyTarget

class NeuroFuzzyErrorImpl : NeuroFuzzyError {

    private val error = NeuroFuzzyOutputErrorImpl()

    override fun invoke(outputs: List<NeuroFuzzyOutput>, target: List<NeuroFuzzyTarget>): Double =
        outputs.zip(target).map { error(it.first, it.second) }.reduce(Double::plus).div(2 * outputs.size)
}