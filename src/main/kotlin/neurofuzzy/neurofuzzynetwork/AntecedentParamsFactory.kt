package neurofuzzy.neurofuzzynetwork

interface AntecedentParamsFactory {

    operator fun invoke(): AntecedentParams
}
