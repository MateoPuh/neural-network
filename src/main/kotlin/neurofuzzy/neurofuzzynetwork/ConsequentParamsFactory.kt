package neurofuzzy.neurofuzzynetwork

interface ConsequentParamsFactory {

    operator fun invoke(size: Int): ConsequentParams
}
