package neurofuzzy.neurofuzzynetwork

import kotlin.random.Random

class RandomConsequentParamsFactory : ConsequentParamsFactory {

    override fun invoke(size: Int): ConsequentParams = ConsequentParams((0 until size).map { Random.nextDouble() * 2 - 1 }.toMutableList(), Random.nextDouble() * 2 - 1)
}
