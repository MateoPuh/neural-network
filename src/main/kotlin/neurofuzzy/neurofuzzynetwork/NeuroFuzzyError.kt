package neurofuzzy.neurofuzzynetwork

import neurofuzzy.data.NeuroFuzzyOutput
import neurofuzzy.data.NeuroFuzzyTarget

interface NeuroFuzzyError {

    operator fun invoke(outputs: List<NeuroFuzzyOutput>, target: List<NeuroFuzzyTarget>): Double
}
