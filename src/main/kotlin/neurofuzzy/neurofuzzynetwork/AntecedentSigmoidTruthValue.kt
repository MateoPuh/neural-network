package neurofuzzy.neurofuzzynetwork

import neurofuzzy.utils.sigmoid

class AntecedentSigmoidTruthValue : AntecedentTruthValue {

    override fun invoke(x: Double, a: Double, b: Double) = (b * (x - a)).sigmoid()
}
