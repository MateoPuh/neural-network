package neurofuzzy.neurofuzzynetwork

interface ConsequentTruthValue {

    operator fun invoke(inputs: List<Double>, params: List<Double>, r: Double): Double
}
