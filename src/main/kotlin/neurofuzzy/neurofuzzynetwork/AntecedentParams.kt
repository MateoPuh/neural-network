package neurofuzzy.neurofuzzynetwork

data class AntecedentParams(var a: Double, var b: Double)
