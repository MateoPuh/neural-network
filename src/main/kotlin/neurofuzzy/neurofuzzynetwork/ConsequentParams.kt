package neurofuzzy.neurofuzzynetwork

data class ConsequentParams(var params: MutableList<Double>, var r: Double)
