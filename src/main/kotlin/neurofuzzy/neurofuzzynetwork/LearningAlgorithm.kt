package neurofuzzy.neurofuzzynetwork

enum class LearningAlgorithm {
    BATCH, STOCHASTIC
}
