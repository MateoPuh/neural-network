package neurofuzzy.neurofuzzynetwork

import neurofuzzy.data.NeuroFuzzyOutput
import neurofuzzy.data.NeuroFuzzyTarget
import kotlin.math.pow

class NeuroFuzzyOutputErrorImpl : NeuroFuzzyOutputError {

    override fun invoke(output: NeuroFuzzyOutput, target: NeuroFuzzyTarget) = (output.output - target.output).pow(2) * 1 / 2
}
