package neurofuzzy.function

object Operations {

    fun zadehNot(): UnaryFunction = UnaryFunction.create { x -> 1 - x }

    fun zadehAnd(): BinaryFunction = BinaryFunction.create(Math::min)

    fun product(): BinaryFunction = BinaryFunction.create { x, y -> x * y }

    fun zadehOr(): BinaryFunction = BinaryFunction.create(Math::max)

    fun hamacherTNorm(theta: Double): BinaryFunction =
        BinaryFunction.create { x, y -> x * y / (theta + (1 - theta) * (x + y - x * y)) }

    fun hamacherSNorm(theta: Double): BinaryFunction =
        BinaryFunction.create { x, y -> (x + y - (2 - theta) * x * y) / (1 - (1 - theta) * x * y) }
}