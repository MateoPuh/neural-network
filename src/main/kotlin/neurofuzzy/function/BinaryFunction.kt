package neurofuzzy.function

interface BinaryFunction {

    operator fun invoke(x: Double, y: Double): Double

    companion object {

        fun create(op: (Double, Double) -> Double) = object : BinaryFunction {

            override fun invoke(x: Double, y: Double): Double = op(x, y)
        }
    }
}
