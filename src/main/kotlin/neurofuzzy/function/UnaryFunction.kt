package neurofuzzy.function

interface UnaryFunction {

    operator fun invoke(x: Double): Double

    companion object {

        fun create(op: (Double) -> Double) = object : UnaryFunction {

            override fun invoke(x: Double): Double = op(x)
        }
    }
}
