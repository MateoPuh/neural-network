package neurofuzzy.data

class NeuroFuzzyOutput(
    val output: Double,
    val pripadnosti: List<Pair<Double, Double>>,
    val w: List<Double>,
    val f: List<Double>
)
