package neurofuzzy.data

data class NeuroFuzzyInput(val inputs: List<Double>) {

    companion object {

        fun from(vararg inputs: Double) = NeuroFuzzyInput(inputs.toList())
    }
}
