package neurofuzzy.data

class NeuroFuzzyTrainingPair(
    val input: NeuroFuzzyInput,
    val output: NeuroFuzzyTarget
)
