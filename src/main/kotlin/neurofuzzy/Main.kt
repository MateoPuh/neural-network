package neurofuzzy

import neurofuzzy.data.NeuroFuzzyInput
import neurofuzzy.data.NeuroFuzzyTrainingPair
import neurofuzzy.neurofuzzynetwork.Anfis
import neurofuzzy.neurofuzzynetwork.LearningAlgorithm
import neurofuzzy.provider.NeuroFuzzyProviderImpl
import neurofuzzy.trainingdata.DataGeneratorFunction
import neurofuzzy.trainingdata.DataLoader
import java.util.*

private const val INPUTS = 2
private const val RULES = 8
private const val MAX_ITERATIONS = 20000
private const val LOG_ITERATIONS = 1000
private const val LEARNING_RATE = 0.0005

private val algorithm = LearningAlgorithm.BATCH

fun main() {
    val provider = NeuroFuzzyProviderImpl()

    val anfis = Anfis(
        numberOfInputs = INPUTS,
        numberOfRules = RULES,
        antecedentParamsFactory = provider.antecedentParamsFactory(),
        consequentParamsFactory = provider.consequentParamsFactory(),
        tNorm = provider.tNorm(),
        antecedentTruthValue = provider.antecedentTruthValue(),
        consequentTruthValue = provider.consequentTruthValue()
    )

    val trainingPairs = DataLoader.loadTrainingData()

    fun getTrainingPairs(iteration: Int, algorithm: LearningAlgorithm): List<NeuroFuzzyTrainingPair> =
        when (algorithm) {
            LearningAlgorithm.STOCHASTIC -> listOf(trainingPairs[iteration % trainingPairs.size])
            LearningAlgorithm.BATCH -> trainingPairs
        }

    println("Iteration,Error")

    for (i in 0..MAX_ITERATIONS) {
        val pairs = getTrainingPairs(i, algorithm)
        val out = pairs.map(NeuroFuzzyTrainingPair::input).map(anfis::output)

        if (i % LOG_ITERATIONS == 0) {
            val outputs = trainingPairs.map(NeuroFuzzyTrainingPair::input).map(anfis::output)
            val err = provider.error().invoke(outputs, trainingPairs.map(NeuroFuzzyTrainingPair::output))
            println(i.toString() + ": " +err)
        }

        anfis.update(
            pairs.map(NeuroFuzzyTrainingPair::input),
            out,
            pairs.map(NeuroFuzzyTrainingPair::output),
            LEARNING_RATE
        )
    }

    val sc = Scanner(System.`in`)

    while (sc.hasNext()) {
        val x = sc.nextDouble()
        val y = sc.nextDouble()

        println(anfis.output(NeuroFuzzyInput.from(x, y)).output)
    }
}
