package neurofuzzy.trainingdata

import neurofuzzy.data.NeuroFuzzyInput
import neurofuzzy.data.NeuroFuzzyTarget
import neurofuzzy.data.NeuroFuzzyTrainingPair

object DataGenerator {

    private val dataGeneratorFunction = DataGeneratorFunction()

    fun generateData(): List<NeuroFuzzyTrainingPair> {
        val xRange = -4..4
        val yRange = -4..4

        return xRange.flatMap { x ->
            yRange.map { y ->
                NeuroFuzzyTrainingPair(
                    NeuroFuzzyInput.from(x.toDouble(), y.toDouble()),
                    NeuroFuzzyTarget(dataGeneratorFunction(x.toDouble(), y.toDouble()))
                )
            }
        }
    }
}
