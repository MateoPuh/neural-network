package neurofuzzy.trainingdata

import neurofuzzy.data.NeuroFuzzyTrainingPair

object DataLoader {

    fun loadTrainingData(): List<NeuroFuzzyTrainingPair> = DataGenerator.generateData()
}
