package neurofuzzy.trainingdata

import neurofuzzy.function.BinaryFunction
import kotlin.math.cos
import kotlin.math.pow

class DataGeneratorFunction : BinaryFunction {

    override fun invoke(x: Double, y: Double): Double = (((x - 1).pow(2)) + ((y + 2).pow(2)) - 5 * x * y + 3) * (cos( x / 5).pow(2))
}
