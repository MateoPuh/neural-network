package neurofuzzy.utils

import kotlin.math.exp

fun Double.sigmoid() = 1.0 / (1.0 + exp(this))
